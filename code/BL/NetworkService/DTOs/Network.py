from dataclasses import dataclass
from DTOs.Container import Container

@dataclass
class Network:
    networkID: str
    networkName: str
    connectedContainers: list
    