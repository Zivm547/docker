from dataclasses import dataclass

@dataclass
class NetworkOptions:
    networkName: str
    driverName: str
    driverOptions: dict