import docker
from  BL.NetworkService.DTOs.Network import Network

class NetworkService:
    def __init__(self, client: docker.DockerClient):
        self._client = client
        
    def createNetwork(self, network_options):
        try:
            newNetwork = self._client.networks.create(
                name=network_options.networkName,
                driver=network_options.driverName
            )
            return Network(networkID=newNetwork.id, 
                           networkName=newNetwork.name, 
                           connectedContainers=newNetwork.containers)
        except Exception as error:
            print(error)
            return False
    
    def connectContainerToNetwork(self, network_name, container_name):
        network: docker.models.networks.Network = self.getNetworkByName(network_name)
        if(network):
            network.connect(container_name)
        
        
    def getNetworkByName(self, network_name):
        try:
            network :docker.models.networks.Network = self._client.networks.get(network_name)
            return network
        except docker.errors.NotFound as notFound:
            print(notFound)
            print(f"Not found Network: {network_name}")
            return False
        except docker.errors.APIError as apiError:
            print(apiError)
            print(f"API error")
            return False