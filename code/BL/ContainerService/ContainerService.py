from  DTOs.Container import *
from  DTOs.Registry import *
import docker


class ContainerService:
    def __init__(self, client):
        self._client = client

    
    def createContainer(self, image_name, container_name, detach=True):
        newContainer = self._client.containers.create(image=image_name, name=container_name, tty=True, detach=detach)
        return Container(newContainer.name, newContainer.id, image_name, newContainer.status)
    
    def startContainer(self, container_name):
        container = self.getContainerByName(container_name)
        if(container):
            container.start()
            
            
    def stopContainer(self, container_name):
        container = self.getContainerByName(container_name)
        if(container):
            container.stop()
        
    
    def executeCommandOnContainer(self, container_name, commands):
        container = self.getContainerByName(container_name)
        if (container):
            container.exec_run(commands)
            
            
    def getAllContainers(self, show_only_running=True):
        try:
            return self._client.containers.list(all= not show_only_running)
        except Exception as error:
            print(error)
            return False
    
    
    def getContainerByName(self, container_name):
        try:
            container :docker.models.containers.Container =  self._client.containers.get(container_name)
            return container
        except docker.errors.NotFound as notFound:
            print(notFound)
            print(f"Not found Container: {container_name}")
            return False
        except docker.errors.APIError as apiError:
            print(apiError)
            print(f"API error")
            return False


        


    