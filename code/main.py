import docker


from  DTOs.Container import *
from  DTOs.Registry import *
from  BL.ContainerService.ContainerService import *
from  BL.NetworkService.NetworkService import *
from  BL.NetworkService.DTOs.NetworkOptions import *

# registry = Registry("tcp://localhost:5000", version="2.0", username="",password="")


def main():
    client = docker.from_env()
    
    service = ContainerService(client)
    networkService = NetworkService(client)
    
    container1 = service.createContainer("alpine", "alpine1")
    container2 = service.createContainer("alpine", "alpine2")
    
    service.startContainer(container1.name)
    service.startContainer(container2.name)
    
    networkOptions = NetworkOptions(networkName="network1", driverName="bridge", driverOptions={})
    network1 = networkService.createNetwork(networkOptions)
    
    networkService.connectContainerToNetwork(network1.networkName, container1.name)
    networkService.connectContainerToNetwork(network1.networkName, container2.name)
    
    # container = service.createContainer("ubuntu:16.04", "ubuntu_container")
    # service.startContainer("ubuntu_container")
    
    # service.executeCommandOnContainer("ubuntu_container","bash")
    # service.stopContainer("ubuntu_container")
    # print(service.getAllContainers())

if (__name__ == "__main__"):
    main()