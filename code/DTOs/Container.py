from dataclasses import dataclass

@dataclass
class Container:
    name: str
    containerID: str
    imageName: str
    status: str
    