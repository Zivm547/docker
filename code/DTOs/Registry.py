from dataclasses import dataclass

@dataclass
class Registry:
    location: str
    version: str
    username: str
    password: str
    